#pragma once

#include <stdint.h>
#include "api_export.h"


#ifdef __cplusplus
extern "C" {
#endif

typedef struct api_error *api_error_t;

API_EXPORT
const char *error_message(api_error_t error);
API_EXPORT
void error_destruct(api_error_t error);


#ifdef __cplusplus
} // extern "C"
#endif