#include "test.h"

#include "dolphinmesh-api/polygonmesh.hpp"

namespace test_dolphinmesh_api {

class PolygonMeshAPI : public ::testing::Test {

public:
  PolygonMesh mesh{6, 9, 1.2};

protected:
  void SetUp() override {
    mesh.add_vertex(0.0, 0.0, 0.0);  // 0
    mesh.add_vertex(1.0, 1.0, 0.0);  // 1
    mesh.add_vertex(2.0, 1.0, 0.0);  // 2
    mesh.add_vertex(3.0, 1.0, 0.0);  // 3
    mesh.add_vertex(4.0, 0.0, 0.0);  // 4
    mesh.add_vertex(3.0, -1.0, 0.0); // 5
    mesh.add_vertex(2.0, -1.0, 0.0); // 6
    mesh.add_vertex(1.0, -1.0, 0.0); // 7
    mesh.add_vertex(2.0, 0.0, 0.0);  // 8

    mesh.add_face(0, 7, 1);
    mesh.add_face(7, 8, 2, 1);
    mesh.add_face(7, 6, 8);
    mesh.add_face(6, 5, 2, 8);
    mesh.add_face(5, 3, 2);
    mesh.add_face(5, 4, 3);

    mesh.finalize();
  }
};

TEST_F(PolygonMeshAPI, properties) {
  EXPECT_EQ(mesh.n_faces(), 6);
  EXPECT_EQ(mesh.n_vertices(), 9);
  EXPECT_EQ(mesh.n_edges(), 14);
  EXPECT_FALSE(mesh.is_closed());
  EXPECT_EQ(mesh.n_genus(), 0);
  EXPECT_EQ(mesh.n_euler(), 1);
  EXPECT_EQ(mesh.n_boundary_components(), 1);
}

TEST_F(PolygonMeshAPI, copy_constructor) { 
  PolygonMesh copy{mesh};  // copy verified manually
  EXPECT_FALSE(copy == mesh);

  EXPECT_EQ(copy.n_faces(), 6);
  EXPECT_EQ(copy.n_vertices(), 9);
  EXPECT_EQ(copy.n_edges(), 14);
  EXPECT_FALSE(copy.is_closed());
  EXPECT_EQ(copy.n_genus(), 0);
  EXPECT_EQ(copy.n_euler(), 1);
  EXPECT_EQ(copy.n_boundary_components(), 1);
}

TEST_F(PolygonMeshAPI, move_constructor) { 
  PolygonMesh move = std::move(mesh);
  EXPECT_FALSE(move == mesh);

  // check if resources are moved
  EXPECT_EQ(mesh.n_faces(), 0);
  EXPECT_EQ(mesh.n_vertices(), 0);
  EXPECT_EQ(mesh.n_edges(), 0);

  EXPECT_EQ(move.n_faces(), 6);
  EXPECT_EQ(move.n_vertices(), 9);
  EXPECT_EQ(move.n_edges(), 14);
  EXPECT_FALSE(move.is_closed());
  EXPECT_EQ(move.n_genus(), 0);
  EXPECT_EQ(move.n_euler(), 1);
  EXPECT_EQ(move.n_boundary_components(), 1);
}

TEST_F(PolygonMeshAPI, copy_assignment) {
  PolygonMesh copy;
  copy = mesh;
  EXPECT_FALSE(copy == mesh);

  EXPECT_EQ(mesh.n_faces(), 6);
  EXPECT_EQ(mesh.n_vertices(), 9);
  EXPECT_EQ(mesh.n_edges(), 14);
  EXPECT_FALSE(mesh.is_closed());
  EXPECT_EQ(mesh.n_genus(), 0);
  EXPECT_EQ(mesh.n_euler(), 1);
  EXPECT_EQ(mesh.n_boundary_components(), 1);

  EXPECT_EQ(copy.n_faces(), 6);
  EXPECT_EQ(copy.n_vertices(), 9);
  EXPECT_EQ(copy.n_edges(), 14);
  EXPECT_FALSE(copy.is_closed());
  EXPECT_EQ(copy.n_genus(), 0);
  EXPECT_EQ(copy.n_euler(), 1);
  EXPECT_EQ(copy.n_boundary_components(), 1);
}

TEST_F(PolygonMeshAPI, move_assignment) {
  PolygonMesh move;
  move = std::move(mesh);
  EXPECT_FALSE(move == mesh);

  // check if resources have moved
  EXPECT_EQ(mesh.n_faces(), 0);
  EXPECT_EQ(mesh.n_vertices(), 0);
  EXPECT_EQ(mesh.n_edges(), 0);

  EXPECT_EQ(move.n_faces(), 6);
  EXPECT_EQ(move.n_vertices(), 9);
  EXPECT_EQ(move.n_edges(), 14);
  EXPECT_FALSE(move.is_closed());
  EXPECT_EQ(move.n_genus(), 0);
  EXPECT_EQ(move.n_euler(), 1);
  EXPECT_EQ(move.n_boundary_components(), 1);
}

TEST_F(PolygonMeshAPI, get_vertex) {

  Vertex a = mesh.get_vertex(0);
  EXPECT_EQ(a.position[0], 0);
  EXPECT_EQ(a.position[1], 0);
  EXPECT_EQ(a.position[2], 0);

  Vertex b = mesh.get_vertex(1);
  EXPECT_EQ(b.position[0], 1);
  EXPECT_EQ(b.position[1], 1);
  EXPECT_EQ(b.position[2], 0);

  Vertex c = mesh.get_vertex(8);
  EXPECT_EQ(c.position[0], 2);
  EXPECT_EQ(c.position[1], 0);
  EXPECT_EQ(c.position[2], 0);
}

TEST_F(PolygonMeshAPI, set_vertex) {

  Vertex a = mesh.get_vertex(0);
  a.position[0] = 0.2;
  a.position[1] = 0.3;
  a.position[2] = 0.4;

  // check if change took effect
  Vertex b = mesh.get_vertex(0);
  EXPECT_EQ(b.position[0], 0.2);
  EXPECT_EQ(b.position[1], 0.3);
  EXPECT_EQ(b.position[2], 0.4);
}

TEST_F(PolygonMeshAPI, get_tri_face) {

  // face properties
  Face f = mesh.get_face(0);
  EXPECT_EQ(f.index, 0);
  EXPECT_EQ(f.n_sides(), 3);

  // check vertices
  Vertex a = f.get_vertex(2);
  EXPECT_EQ(a.position[0], 1);
  EXPECT_EQ(a.position[1], 1);
  EXPECT_EQ(a.position[2], 0);

  // check if mesh can be modified
  a.position[0] = 0.9;
  a.position[1] = 0.8;
  a.position[2] = 0.1;
  EXPECT_EQ(mesh.get_vertex(1).position[0], 0.9);
  EXPECT_EQ(mesh.get_vertex(1).position[1], 0.8);
  EXPECT_EQ(mesh.get_vertex(1).position[2], 0.1);
}

TEST_F(PolygonMeshAPI, get_quad_face) {

  // face properties
  Face f = mesh.get_face(1);
  EXPECT_EQ(f.index, 1);
  EXPECT_EQ(f.n_sides(), 4);

  // check vertices
  Vertex a = f.get_vertex(2);
  EXPECT_EQ(a.position[0], 1);
  EXPECT_EQ(a.position[1], 1);
  EXPECT_EQ(a.position[2], 0);

  // check if mesh can be modified
  a.position[0] = 0.9;
  a.position[1] = 0.8;
  a.position[2] = 0.1;
  EXPECT_EQ(mesh.get_vertex(1).position[0], 0.9);
  EXPECT_EQ(mesh.get_vertex(1).position[1], 0.8);
  EXPECT_EQ(mesh.get_vertex(1).position[2], 0.1);
}

} // namespace test_dolphinmesh_api