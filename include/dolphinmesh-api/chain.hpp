#pragma once

#include <assert.h>
#include "chain.h"
#include "primitives.hpp"


class Chain {

private:
   chain_t _opaque;

 public:

  Chain() = delete;

  Chain(const chain_t& c) : _opaque(c) {}

  ~Chain() { chain_destruct(_opaque); }

  const bool is_closed() const { return chain_is_closed(_opaque); }

  const std::size_t n_vertices() const { return chain_n_vertices(_opaque); }

  const std::size_t n_edges() const { return chain_n_edges(_opaque); }

  Vertex get_vertex(const int32_t index) const {
    ArrayView<double> p{chain_get_vertex_position_ptr(_opaque, index), 3};
    return Vertex(index, p);
  }

  Edge get_edge(const int32_t index) const {
    if (index == n_edges()-1 && is_closed()) {
      Vertex a = get_vertex(index);
      Vertex b = get_vertex(0);
      return Edge(index, a, b);
    } else {
      Vertex a = get_vertex(index);
      Vertex b = get_vertex(index+1);
      return Edge(index, a, b); 
    }
  }
};