#pragma once

#include "trimesh.h"
#include "error.hpp"

#include "primitives.hpp"
#include "chain.hpp"

class TriMesh {

private:
  trimesh_t _opaque;
 
public:
  /// <summary>
  /// Default constructor
  /// </summary>
  TriMesh() : _opaque(trimesh_default_constructor(ThrowOnError{})) {}

  /// <summary>
  /// Constructor
  /// </summary>
  /// <param name="n_faces"></param>
  /// <param name="n_vertices"></param>
  /// <param name="storage_factor"></param>
  TriMesh(const std::size_t n_faces, const std::size_t n_vertices,
              const double storage_factor)
      : _opaque(trimesh_construct_f1(n_faces, n_vertices, storage_factor,
                                         ThrowOnError{})) {}

  // destructor
  ~TriMesh() { trimesh_destruct(_opaque);
  }

  /// <summary>
  /// Copy constructor. Performs a deep-copy.
  /// </summary>
  /// <param name=""></param>
  TriMesh(const TriMesh &other)
      : _opaque(trimesh_copy_constructor(other._opaque, ThrowOnError{})){};

  /// <summary>
  /// Move constructor. Moves the resourses of `other` to `this` mesh.
  /// </summary>
  /// <param name="other"></param>
  TriMesh(TriMesh &&other)
      : _opaque(trimesh_move_constructor(other._opaque, ThrowOnError{})){};

  /// <summary>
  /// Copy assignment operator
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  TriMesh &operator=(const TriMesh &other) {
    trimesh_copy_assignment(this->_opaque, other._opaque, ThrowOnError{});
    return *this;
  }

  /// <summary>
  /// Move assignment operator
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  TriMesh &operator=(TriMesh &&other) {
    trimesh_move_assignment(this->_opaque, other._opaque, ThrowOnError{});
    return *this;
  }

  /// <summary>
  /// Check if two meshes are strongly equal, meaning they share exactly
  /// the same data.
  /// </summary>
  /// <param name="mesh"></param>
  /// <param name="other"></param>
  /// <returns></returns>
  friend bool operator==(const TriMesh &mesh, const TriMesh &other) {
    return trimesh_same_objects(mesh._opaque, other._opaque,
                                    ThrowOnError{});
  }

  /// <summary>
  /// Return number of mesh vertices
  /// </summary>
  /// <returns></returns>
  const uint32_t n_vertices() const { return trimesh_n_vertices(_opaque);
  }

  /// <summary>
  /// Return number of mesh faces
  /// </summary>
  /// <returns></returns>
  const uint32_t n_faces() const { return trimesh_n_faces(_opaque);
  }

  /// <summary>
  /// Return number of mesh edges
  /// </summary>
  /// <returns></returns>
  const uint32_t n_edges() const { return trimesh_n_edges(_opaque);
  }

  /// <summary>
  /// Return number of boundary components
  /// </summary>
  /// <returns></returns>
  const uint32_t n_boundary_components() const {
    return trimesh_n_boundary_components(_opaque);
  }

  /// <summary>
  /// Compute the surface genus
  /// </summary>
  /// <returns></returns>
  const uint32_t n_genus() const { return trimesh_n_genus(_opaque);
  }

  /// <summary>
  /// Compute the mesh Euler number
  /// </summary>
  /// <returns></returns>
  const uint32_t n_euler() const { return trimesh_n_euler(_opaque); }

  /// <summary>
  /// Returns true if the mesh is a closed surface
  /// </summary>
  /// <returns></returns>
  const bool is_closed() const { return trimesh_is_closed(_opaque); }

  /// <summary>
  /// Add mesh vertex
  /// </summary>
  /// <param name="x"></param>
  /// <param name="y"></param>
  /// <param name="z"></param>
  void add_vertex(double x, double y, double z) {
    return trimesh_add_vertex(_opaque, x, y, z, ThrowOnError{});
  }

  /// <summary>
  /// Add mesh face
  /// </summary>
  /// <param name="a"></param>
  /// <param name="b"></param>
  /// <param name="c"></param>
  void add_face(int32_t a, int32_t b, int32_t c) {
    return trimesh_add_face_3(this->_opaque, a, b, c, ThrowOnError{});
  }

  /// <summary>
  /// Finalize initialization of mesh object. This function needs to be called
  /// after all vertices and faces have been added and before the mesh is used
  /// in further operations.
  /// </summary>
  void finalize() { return trimesh_finalize(_opaque, ThrowOnError{});
  }

  /// <summary>
  /// Get vertex
  /// </summary>
  /// <param name="index"></param>
  /// <returns></returns>
  Vertex get_vertex(const int32_t index) const {
    ArrayView<double> p{trimesh_get_vertex_position_ptr(_opaque, index), 3};
    return Vertex{index, p};
  }

  /// <summary>
  /// Get Face
  /// </summary>
  /// <param name="index"></param>
  /// <returns></returns>
  Face get_face(const int32_t index) const {
    int32_t face_vertices[3];
    trimesh_get_face_vertices_3(_opaque, index, face_vertices, ThrowOnError{});
    const Vertex &a = get_vertex(face_vertices[0]);
    const Vertex &b = get_vertex(face_vertices[1]);
    const Vertex &c = get_vertex(face_vertices[2]);
    return Face{index, std::move(a), std::move(b), std::move(c)};
  }

  /// <summary>
  /// Get Edge
  /// </summary>
  /// <param name="index"></param>
  /// <returns></returns>
  Edge get_edge(const int32_t index) const {
    int32_t edge_vertices[2];
    trimesh_get_edge_vertices(_opaque, index, edge_vertices, ThrowOnError{});
    const Vertex &a = get_vertex(edge_vertices[0]);
    const Vertex &b = get_vertex(edge_vertices[1]);
    return Edge{index, a, b};
  }

  /// <summary>
  /// Get the `ith` boundary component
  /// </summary>
  /// <param name="index"></param>
  /// <returns></returns>
  Chain get_boundary_component(const int32_t index) const {
    return Chain{trimesh_get_boundary_component(_opaque, index, ThrowOnError{})};
  }

};