#include "test.h"

#include "dolphinmesh-api/trimesh.hpp"
#include "dolphinmesh-api/chain.hpp"
#include "dolphinmesh-api/primitives.hpp"

namespace test_trimesh_api {

class TriMeshAPI : public ::testing::Test {

public:
  TriMesh mesh{3, 4, 1.2}; // tetrahydron with boundary

protected:
  void SetUp() override {
    mesh.add_vertex(0.0, 0.0, 0.0);  // 0
    mesh.add_vertex(1.0, 0.0, 0.0);  // 1
    mesh.add_vertex(0.0, 1.0, 0.0);  // 2
    mesh.add_vertex(0.0, 0.0, 1.0);  // 3

    mesh.add_face(0, 2, 1);
    mesh.add_face(0, 1, 3);
    mesh.add_face(1, 2, 3);

    mesh.finalize();
  }
};

TEST_F(TriMeshAPI, properties) {
  EXPECT_EQ(mesh.n_faces(), 3);
  EXPECT_EQ(mesh.n_vertices(), 4);
  EXPECT_EQ(mesh.n_edges(), 6);
  EXPECT_FALSE(mesh.is_closed());
  EXPECT_EQ(mesh.n_genus(), 0);
  EXPECT_EQ(mesh.n_euler(), 1);
  EXPECT_EQ(mesh.n_boundary_components(), 1);
}

TEST_F(TriMeshAPI, copy_constructor) {
  TriMesh copy{mesh}; // copy verified manually
  EXPECT_FALSE(copy == mesh);

  // check that `mesh` is not moved from
  EXPECT_EQ(mesh.n_faces(), 3);
  EXPECT_EQ(mesh.n_vertices(), 4);
  EXPECT_EQ(mesh.n_edges(), 6);
  EXPECT_FALSE(mesh.is_closed());
  EXPECT_EQ(mesh.n_genus(), 0);
  EXPECT_EQ(mesh.n_euler(), 1);
  EXPECT_EQ(mesh.n_boundary_components(), 1);

  // check copy
  EXPECT_EQ(copy.n_faces(), 3);
  EXPECT_EQ(copy.n_vertices(), 4);
  EXPECT_EQ(copy.n_edges(), 6);
  EXPECT_FALSE(copy.is_closed());
  EXPECT_EQ(copy.n_genus(), 0);
  EXPECT_EQ(copy.n_euler(), 1);
  EXPECT_EQ(copy.n_boundary_components(), 1);
}

TEST_F(TriMeshAPI, move_constructor) { 
  TriMesh move = std::move(mesh);
  EXPECT_FALSE(move == mesh);

  // check if resources have been moved
  EXPECT_EQ(mesh.n_faces(), 0);
  EXPECT_EQ(mesh.n_vertices(), 0);
  EXPECT_EQ(mesh.n_edges(), 0);

  // check moved obj
  EXPECT_EQ(move.n_faces(), 3);
  EXPECT_EQ(move.n_vertices(), 4);
  EXPECT_EQ(move.n_edges(), 6);
  EXPECT_FALSE(move.is_closed());
  EXPECT_EQ(move.n_genus(), 0);
  EXPECT_EQ(move.n_euler(), 1);
  EXPECT_EQ(move.n_boundary_components(), 1);
}

TEST_F(TriMeshAPI, copy_assignment) {
  TriMesh copy;
  copy = mesh;
  EXPECT_FALSE(copy == mesh);

  // check that `mesh` is not moved from
  EXPECT_EQ(mesh.n_faces(), 3);
  EXPECT_EQ(mesh.n_vertices(), 4);
  EXPECT_EQ(mesh.n_edges(), 6);
  EXPECT_FALSE(mesh.is_closed());
  EXPECT_EQ(mesh.n_genus(), 0);
  EXPECT_EQ(mesh.n_euler(), 1);
  EXPECT_EQ(mesh.n_boundary_components(), 1);

  // check copy
  EXPECT_EQ(copy.n_faces(), 3);
  EXPECT_EQ(copy.n_vertices(), 4);
  EXPECT_EQ(copy.n_edges(), 6);
  EXPECT_FALSE(copy.is_closed());
  EXPECT_EQ(copy.n_genus(), 0);
  EXPECT_EQ(copy.n_euler(), 1);
  EXPECT_EQ(copy.n_boundary_components(), 1);
}

TEST_F(TriMeshAPI, move_assignment) {
  TriMesh move;
  move = std::move(mesh);
  EXPECT_FALSE(move == mesh);

  // check if resources have moved
  EXPECT_EQ(mesh.n_faces(), 0);
  EXPECT_EQ(mesh.n_vertices(), 0);
  EXPECT_EQ(mesh.n_edges(), 0);

  // check moved obj
  EXPECT_EQ(move.n_faces(), 3);
  EXPECT_EQ(move.n_vertices(), 4);
  EXPECT_EQ(move.n_edges(), 6);
  EXPECT_FALSE(move.is_closed());
  EXPECT_EQ(move.n_genus(), 0);
  EXPECT_EQ(move.n_euler(), 1);
  EXPECT_EQ(move.n_boundary_components(), 1);
}

TEST_F(TriMeshAPI, get_vertex) {

  Vertex a = mesh.get_vertex(0);
  EXPECT_EQ(a.index, 0);
  EXPECT_EQ(a.position[0], 0);
  EXPECT_EQ(a.position[1], 0);
  EXPECT_EQ(a.position[2], 0);

  Vertex b = mesh.get_vertex(1);
  EXPECT_EQ(b.index, 1);
  EXPECT_EQ(b.position[0], 1);
  EXPECT_EQ(b.position[1], 0);
  EXPECT_EQ(b.position[2], 0);

  Vertex c = mesh.get_vertex(2);
  EXPECT_EQ(c.index, 2);
  EXPECT_EQ(c.position[0], 0);
  EXPECT_EQ(c.position[1], 1);
  EXPECT_EQ(c.position[2], 0);
  
  Vertex d = mesh.get_vertex(3);
  EXPECT_EQ(d.index, 3);
  EXPECT_EQ(d.position[0], 0);
  EXPECT_EQ(d.position[1], 0);
  EXPECT_EQ(d.position[2], 1);
}

TEST_F(TriMeshAPI, set_vertex) {

  Vertex a = mesh.get_vertex(0);
  a.position[0] = 0.2;
  a.position[1] = 0.3;
  a.position[2] = 0.4;

  // check if change took effect
  Vertex b = mesh.get_vertex(0);
  EXPECT_EQ(b.position[0], 0.2);
  EXPECT_EQ(b.position[1], 0.3);
  EXPECT_EQ(b.position[2], 0.4);
}

#include <iostream>
#include <cstdlib> 

TEST_F(TriMeshAPI, get_face) { 

    // face properties
    Face f = mesh.get_face(1); 
    EXPECT_EQ(f.index, 1);
    EXPECT_EQ(f.n_sides(), 3);

    // // check vertices
    Vertex a = f.get_vertex(2);
    EXPECT_EQ(a.index, 3);
    EXPECT_EQ(a.position[0], 0);
    EXPECT_EQ(a.position[1], 0);
    EXPECT_EQ(a.position[2], 1);

    // check if mesh can be modified
    a.position[0] = 0.9;
    a.position[1] = 0.1;
    a.position[2] = 0.2;
    EXPECT_EQ(mesh.get_vertex(3).position[0], 0.9);
    EXPECT_EQ(mesh.get_vertex(3).position[1], 0.1);
    EXPECT_EQ(mesh.get_vertex(3).position[2], 0.2);
}

TEST_F(TriMeshAPI, get_edge) {

  Edge e = mesh.get_edge(0);

  Vertex va = e.get_vertex(0);
  EXPECT_EQ(va.position[0], 0);
  EXPECT_EQ(va.position[1], 0);
  EXPECT_EQ(va.position[2], 0);

  Vertex vb = e.get_vertex(1);
  EXPECT_EQ(vb.position[0], 1);
  EXPECT_EQ(vb.position[1], 0);
  EXPECT_EQ(vb.position[2], 0);

  // check if mesh can be modified
  e.get_vertex(0).position[0] = 0.3;
  e.get_vertex(0).position[1] = 0.1;
  e.get_vertex(0).position[2] = 0.2;
  EXPECT_EQ(mesh.get_vertex(0).position[0], 0.3);
  EXPECT_EQ(mesh.get_vertex(0).position[1], 0.1);
  EXPECT_EQ(mesh.get_vertex(0).position[2], 0.2);

}

TEST_F(TriMeshAPI, get_boundary) {

  EXPECT_EQ(mesh.n_boundary_components(), 1);

  // Check boundary component
  Chain c = mesh.get_boundary_component(0);
  EXPECT_EQ(c.n_vertices(), 3);
  EXPECT_EQ(c.n_edges(), 3);
  EXPECT_TRUE(c.is_closed());

  // check edge 0
  EXPECT_EQ(c.get_vertex(0), c.get_edge(0).get_vertex(0));
  EXPECT_EQ(c.get_vertex(1), c.get_edge(0).get_vertex(1));

  // heck edge 1
  EXPECT_EQ(c.get_vertex(1), c.get_edge(1).get_vertex(0));
  EXPECT_EQ(c.get_vertex(2), c.get_edge(1).get_vertex(1));

  // check edge 2
  EXPECT_EQ(c.get_vertex(2), c.get_edge(2).get_vertex(0));
  EXPECT_EQ(c.get_vertex(0), c.get_edge(2).get_vertex(1));
}

} // namespace test_trimesh_api