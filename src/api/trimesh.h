#pragma once

#include <stdint.h>
#include "api_export.h"

#include "error.h"
#include "chain.h"

#ifdef __cplusplus
extern "C" {
#endif

///
/// Alias to polgonmesh* 
///
typedef struct trimesh *trimesh_t;

///
/// Constructors 
///
API_EXPORT
trimesh_t trimesh_default_constructor(api_error_t *);

API_EXPORT
trimesh_t trimesh_construct_f1(int32_t, int32_t, double, api_error_t *);

API_EXPORT
trimesh_t trimesh_copy_constructor(trimesh_t, api_error_t *);

API_EXPORT
trimesh_t trimesh_move_constructor(trimesh_t, api_error_t *);

API_EXPORT
void trimesh_copy_assignment(trimesh_t, trimesh_t, api_error_t *);

API_EXPORT
void trimesh_move_assignment(trimesh_t, trimesh_t, api_error_t *);

///
/// Destructo object
///
API_EXPORT
void trimesh_destruct(trimesh_t);

///
/// Mesh properties
///
API_EXPORT
bool trimesh_same_objects(trimesh_t, trimesh_t, api_error_t *);

API_EXPORT
int32_t trimesh_n_vertices(trimesh_t);

API_EXPORT
int32_t trimesh_n_faces(trimesh_t);

API_EXPORT
int32_t trimesh_n_edges(trimesh_t);

API_EXPORT
int32_t trimesh_n_boundary_components(trimesh_t);

API_EXPORT
int32_t trimesh_n_genus(trimesh_t);

API_EXPORT
int32_t trimesh_n_euler(trimesh_t);

API_EXPORT
bool trimesh_is_closed(trimesh_t);

///
/// Add mesh vertex
///
API_EXPORT
void trimesh_add_vertex(trimesh_t, double, double, double, api_error_t *);

///
/// Add mesh face
///
API_EXPORT
void trimesh_add_face_3(trimesh_t, int32_t, int32_t, int32_t, api_error_t *);

///
/// Finalize mesh object
///
API_EXPORT
void trimesh_finalize(trimesh_t, api_error_t *);

///
/// Get the position vector corresponding to given vertex
///
API_EXPORT
void trimesh_get_vertex_position(trimesh_t, int32_t, double*, api_error_t *);

///
/// Get a pointer to vertex position
///
API_EXPORT
double *trimesh_get_vertex_position_ptr(trimesh_t, int32_t);

///
/// Get the vertex indices of a face
///
API_EXPORT
void trimesh_get_face_vertices_3(trimesh_t, int32_t, int32_t *, api_error_t *);

///
/// Get the vertex indices of an edge
///
API_EXPORT
void trimesh_get_edge_vertices(trimesh_t, int32_t, int32_t *, api_error_t *);

///
/// Get a boundary component 
///
API_EXPORT
chain_t trimesh_get_boundary_component(trimesh_t, int32_t, api_error_t *);

#ifdef __cplusplus
} // extern "C"
#endif
