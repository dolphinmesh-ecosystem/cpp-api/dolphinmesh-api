#include "trimesh.hpp"
#include <iostream>

int main() {

  TriMesh mesh{6, 9, 1.2};

  mesh.add_vertex(0.0, 0.0, 0.0); // 0
  mesh.add_vertex(1.0, 0.0, 0.0); // 1
  mesh.add_vertex(0.0, 1.0, 0.0); // 2
  mesh.add_vertex(0.0, 0.0, 1.0); // 3

  mesh.add_face(0, 2, 1);
  mesh.add_face(0, 1, 3);
  mesh.add_face(1, 2, 3);
  mesh.add_face(0, 3, 2);

  mesh.finalize();

  std::cout << "mesh properties: " << std::endl;
  std::cout << "number of vertices: " << mesh.n_vertices() << std::endl;
  std::cout << "number of faces: " << mesh.n_faces() << std::endl;
  std::cout << "number of edges: " << mesh.n_edges() << std::endl;
  std::cout << "number of boundaries: " << mesh.n_boundary_components()
            << std::endl;
  std::cout << "mesh genus: " << mesh.n_genus() << std::endl;
}