#pragma once

#include <stdexcept>

#include "error.h"

struct Error {
  Error() : opaque(nullptr) {}

  ~Error() {
    if (opaque) {
      error_destruct(opaque);
    }
  }

  api_error_t opaque;
};

class ThrowOnError {
public:
  ~ThrowOnError() noexcept(false) {
    if (_error.opaque) {
      throw std::runtime_error(error_message(_error.opaque));
    }
  }

  operator api_error_t *() { return &_error.opaque; }

private:
  Error _error;
};