#pragma once

#include <stdint.h>

#include <string>
#include "api_export.h"
#include "error.h"
#include "primitives.h"

#ifdef __cplusplus
extern "C" {
#endif

///
/// Alias to polgonmesh* 
///
typedef struct polygonmesh *polygonmesh_t;

API_EXPORT
void polygonmesh_print_hello();

///
/// Constructors 
///
API_EXPORT
polygonmesh_t polygonmesh_default_constructor(api_error_t *);

API_EXPORT
polygonmesh_t polygonmesh_construct_f1(int32_t, int32_t, double, api_error_t *);

API_EXPORT
polygonmesh_t polygonmesh_copy_constructor(polygonmesh_t, api_error_t *);

API_EXPORT
polygonmesh_t polygonmesh_move_constructor(polygonmesh_t, api_error_t *);

API_EXPORT
void polygonmesh_copy_assignment(polygonmesh_t, polygonmesh_t, api_error_t *);

API_EXPORT
void polygonmesh_move_assignment(polygonmesh_t, polygonmesh_t, api_error_t *);

///
/// Destructo object
///
API_EXPORT
void polygonmesh_destruct(polygonmesh_t);

///
/// Mesh properties
///
API_EXPORT
bool polygonmesh_same_objects(polygonmesh_t, polygonmesh_t, api_error_t *);

API_EXPORT
int32_t polygonmesh_n_vertices(polygonmesh_t);

API_EXPORT
int32_t polygonmesh_n_faces(polygonmesh_t);

API_EXPORT
int32_t polygonmesh_n_edges(polygonmesh_t);

API_EXPORT
int32_t polygonmesh_n_boundary_components(polygonmesh_t);

API_EXPORT
int32_t polygonmesh_n_genus(polygonmesh_t);

API_EXPORT
int32_t polygonmesh_n_euler(polygonmesh_t);

API_EXPORT
bool polygonmesh_is_closed(polygonmesh_t);

///
/// Add mesh vertex
///
API_EXPORT
void polygonmesh_add_vertex(polygonmesh_t, double, double, double, api_error_t *);

///
/// Add mesh face
///
API_EXPORT
void polygonmesh_add_face_3(polygonmesh_t, int32_t, int32_t,
                            int32_t, api_error_t *);

API_EXPORT
void polygonmesh_add_face_4(polygonmesh_t, int32_t, int32_t,
                            int32_t, int32_t, api_error_t *);

API_EXPORT
void polygonmesh_add_face_5(polygonmesh_t, int32_t, 
                            int32_t, int32_t, 
                            int32_t, int32_t, 
                            api_error_t *);

///
/// Finalize mesh object
///
API_EXPORT
void polygonmesh_finalize(polygonmesh_t, api_error_t *);

///
/// Get the position vector corresponding to given vertex
///
API_EXPORT
void polygonmesh_get_vertex_position(polygonmesh_t, int32_t, point3d_t, api_error_t *);

///
/// Get the number of vertices of a face
///
API_EXPORT
int32_t polygonmesh_n_face_vertices(polygonmesh_t, int32_t, api_error_t *);

///
/// Get the vertex indices of a face
///
API_EXPORT
void polygonmesh_get_face_vertices_3(polygonmesh_t, int32_t, int32_t*, api_error_t *);

API_EXPORT
void polygonmesh_get_face_vertices_4(polygonmesh_t, int32_t, int32_t *,
                                     api_error_t *);

API_EXPORT
void polygonmesh_get_face_vertices_5(polygonmesh_t, int32_t, int32_t *,
                                     api_error_t *);

///
/// Get the vertex indices of an edge
///
API_EXPORT
void polygonmesh_get_edge_vertices(polygonmesh_t, int32_t, int32_t *, api_error_t *);


#ifdef __cplusplus
} // extern "C"
#endif
