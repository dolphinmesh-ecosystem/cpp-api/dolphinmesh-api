#pragma once

#include "polygonmesh.h"
#include "error.hpp"

#include "primitives.hpp"

class PolygonMesh {

private:
  polygonmesh_t _opaque;
 
public:
  /// <summary>
  /// Default constructor
  /// </summary>
  PolygonMesh() : _opaque(polygonmesh_default_constructor(ThrowOnError{})) {}

  /// <summary>
  /// Constructor
  /// </summary>
  /// <param name="n_faces"></param>
  /// <param name="n_vertices"></param>
  /// <param name="storage_factor"></param>
  PolygonMesh(const std::size_t n_faces, const std::size_t n_vertices,
              const double storage_factor)
      : _opaque(polygonmesh_construct_f1(n_faces, n_vertices, storage_factor,
                                         ThrowOnError{})) {}

  // destructor
  ~PolygonMesh() { polygonmesh_destruct(_opaque);
  }

  /// <summary>
  /// Copy constructor. Performs a deep-copy.
  /// </summary>
  /// <param name=""></param>
  PolygonMesh(const PolygonMesh &other)
      : _opaque(polygonmesh_copy_constructor(other._opaque, ThrowOnError{})){};

  /// <summary>
  /// Move constructor. Moves the resourses of `other` to `this` mesh.
  /// </summary>
  /// <param name="other"></param>
  PolygonMesh(PolygonMesh &&other)
      : _opaque(polygonmesh_move_constructor(other._opaque, ThrowOnError{})){};

  /// <summary>
  /// Copy assignment operator
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  PolygonMesh &operator=(const PolygonMesh &other) {
    polygonmesh_copy_assignment(this->_opaque, other._opaque, ThrowOnError{});
    return *this;
  }

  /// <summary>
  /// Move assignment operator
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  PolygonMesh &operator=(PolygonMesh &&other) {
    polygonmesh_move_assignment(this->_opaque, other._opaque, ThrowOnError{});
    return *this;
  }

  /// <summary>
  /// Check if two meshes are strongly equal, meaning they share exactly
  /// the same data.
  /// </summary>
  /// <param name="mesh"></param>
  /// <param name="other"></param>
  /// <returns></returns>
  friend bool operator==(const PolygonMesh &mesh, const PolygonMesh &other) {
    return polygonmesh_same_objects(mesh._opaque, other._opaque,
                                    ThrowOnError{});
  }

  /// <summary>
  /// Return number of mesh vertices
  /// </summary>
  /// <returns></returns>
  const uint32_t n_vertices() const { return polygonmesh_n_vertices(_opaque);
  }

  /// <summary>
  /// Return number of mesh faces
  /// </summary>
  /// <returns></returns>
  const uint32_t n_faces() const { return polygonmesh_n_faces(_opaque);
  }

  /// <summary>
  /// Return number of mesh edges
  /// </summary>
  /// <returns></returns>
  const uint32_t n_edges() const { return polygonmesh_n_edges(_opaque);
  }

  /// <summary>
  /// Return number of boundary components
  /// </summary>
  /// <returns></returns>
  const uint32_t n_boundary_components() const {
    return polygonmesh_n_boundary_components(_opaque);
  }

  /// <summary>
  /// Compute the surface genus
  /// </summary>
  /// <returns></returns>
  const uint32_t n_genus() const { return polygonmesh_n_genus(_opaque);
  }

  /// <summary>
  /// Compute the mesh Euler number
  /// </summary>
  /// <returns></returns>
  const uint32_t n_euler() const { return polygonmesh_n_euler(_opaque); }

  /// <summary>
  /// Returns true if the mesh is a closed surface
  /// </summary>
  /// <returns></returns>
  const bool is_closed() const { return polygonmesh_is_closed(_opaque); }

  /// <summary>
  /// Add mesh vertex
  /// </summary>
  /// <param name="x"></param>
  /// <param name="y"></param>
  /// <param name="z"></param>
  void add_vertex(double x, double y, double z) {
    return polygonmesh_add_vertex(_opaque, x, y, z, ThrowOnError{});
  }

  /// <summary>
  /// Add mesh face
  /// </summary>
  /// <param name="a"></param>
  /// <param name="b"></param>
  /// <param name="c"></param>
  void add_face(int64_t a, int64_t b, int64_t c) {
    return polygonmesh_add_face_3(this->_opaque, a, b, c, ThrowOnError{});
  }

  /// <summary>
  /// Add mesh face
  /// </summary>
  /// <param name="a"></param>
  /// <param name="b"></param>
  /// <param name="c"></param>
  /// <param name="d"></param>
  void add_face(int64_t a, int64_t b, int64_t c, int64_t d) {
    return polygonmesh_add_face_4(this->_opaque, a, b, c, d, ThrowOnError{});
  }

  /// <summary>
  /// Add mesh face
  /// </summary>
  /// <param name="a"></param>
  /// <param name="b"></param>
  /// <param name="c"></param>
  /// <param name="d"></param>
  /// <param name="e"></param>
  void add_face(int64_t a, int64_t b, int64_t c, int64_t d, int64_t e) {
    return polygonmesh_add_face_5(this->_opaque, a, b, c, d, e, ThrowOnError{});
  }

  /// <summary>
  /// Finalize initialization of mesh object. This function needs to be called
  /// after all vertices and faces have been added and before the mesh is used
  /// in further operations.
  /// </summary>
  void finalize() { return polygonmesh_finalize(_opaque, ThrowOnError{});
  }

      /// <summary>
  /// Get vertex
  /// </summary>
  /// <param name="index"></param>
  /// <returns></returns>
  Vertex get_vertex(const int32_t index) const {
    point3d pt;
    polygonmesh_get_vertex_position(_opaque, index, &pt, ThrowOnError{});
    Point3d point;
    point.set_data_ptr(pt.data);
    return Vertex(index, point);
  }

  /// <summary>
  /// Get Face
  /// </summary>
  /// <param name="index"></param>
  /// <returns></returns>
  Face get_face(const int32_t index) const {

    auto n_sides = polygonmesh_n_face_vertices(_opaque, index, ThrowOnError{});

    if (n_sides == 3) {
      int32_t face_vertices[3];
      polygonmesh_get_face_vertices_3(_opaque, index, face_vertices,
                                      ThrowOnError{});
      Vertex a = get_vertex(face_vertices[0]);
      Vertex b = get_vertex(face_vertices[1]);
      Vertex c = get_vertex(face_vertices[2]);
      return Face(index, a, b, c);
    } 
    else if (n_sides == 4) {
      int32_t face_vertices[4];
      polygonmesh_get_face_vertices_4(_opaque, index, face_vertices,
                                      ThrowOnError{});
      Vertex a = get_vertex(face_vertices[0]);
      Vertex b = get_vertex(face_vertices[1]);
      Vertex c = get_vertex(face_vertices[2]);
      Vertex d = get_vertex(face_vertices[3]);
      return Face(index, a, b, c, d);
    } 
    else if (n_sides == 5) {
      int32_t face_vertices[5];
      polygonmesh_get_face_vertices_5(_opaque, index, face_vertices,
                                      ThrowOnError{});
      Vertex a = get_vertex(face_vertices[0]);
      Vertex b = get_vertex(face_vertices[1]);
      Vertex c = get_vertex(face_vertices[2]);
      Vertex d = get_vertex(face_vertices[3]);
      Vertex e = get_vertex(face_vertices[4]);
      return Face(index, a, b, c, d, e);
    }
  }

  /// <summary>
  /// Get Edge
  /// </summary>
  /// <param name="index"></param>
  /// <returns></returns>
  Edge get_edge(const int32_t index) const {
    int32_t edge_vertices[2];
    polygonmesh_get_edge_vertices(_opaque, index, edge_vertices, ThrowOnError{});
    Vertex a = get_vertex(edge_vertices[0]);
    Vertex b = get_vertex(edge_vertices[1]);
    return Edge(index, a, b);
  }

};