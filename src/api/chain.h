#pragma once

#include "api_export.h"
#include <stdint.h>

#include "error.h"

#ifdef __cplusplus
extern "C" {
#endif

///
/// Alias to polgonmesh*
///
typedef struct chain *chain_t;

API_EXPORT
void chain_destruct(chain_t);

API_EXPORT
bool chain_is_closed(chain_t);

API_EXPORT 
int32_t chain_n_vertices(chain_t);

API_EXPORT
int32_t chain_n_edges(chain_t);

API_EXPORT
double *chain_get_vertex_position_ptr(chain_t, int32_t);


#ifdef __cplusplus
} // extern "C"
#endif
