#include "test.h"

#include "dolphinmesh-api/quadmesh.hpp"

namespace test_dolphinmesh_api {

class QuadMeshAPI : public ::testing::Test {

public:
  QuadMesh mesh{6, 8, 1.2};

protected:
  void SetUp() override {

    mesh.add_vertex(0.0, 0.0, 0.0); // 0
    mesh.add_vertex(1.0, 0.0, 0.0); // 1
    mesh.add_vertex(1.0, 1.0, 0.0); // 2
    mesh.add_vertex(0.0, 1.0, 0.0); // 3
    mesh.add_vertex(0.0, 0.0, 1.0); // 4
    mesh.add_vertex(1.0, 0.0, 1.0); // 5
    mesh.add_vertex(1.0, 1.0, 1.0); // 6
    mesh.add_vertex(0.0, 1.0, 1.0); // 7

    mesh.add_face(0, 3, 2, 1); // 1
    mesh.add_face(0, 1, 5, 4); // 2
    mesh.add_face(1, 2, 6, 5); // 3
    mesh.add_face(2, 3, 7, 6); // 4
    mesh.add_face(0, 4, 7, 3); // 5
    mesh.add_face(4, 5, 6, 7); // 6

    mesh.finalize();
  }
};

TEST_F(QuadMeshAPI, properties) {
  EXPECT_EQ(mesh.n_faces(), 6);
  EXPECT_EQ(mesh.n_vertices(), 8);
  EXPECT_EQ(mesh.n_edges(), 12);
  EXPECT_TRUE(mesh.is_closed());
  EXPECT_EQ(mesh.n_genus(), 0);
  EXPECT_EQ(mesh.n_euler(), 2);
  EXPECT_EQ(mesh.n_boundary_components(), 0);
}

TEST_F(QuadMeshAPI, copy_constructor) { 
  QuadMesh copy{mesh};  // copy verified manually
  EXPECT_FALSE(copy == mesh);

  // check original
  EXPECT_EQ(mesh.n_faces(), 6);
  EXPECT_EQ(mesh.n_vertices(), 8);
  EXPECT_EQ(mesh.n_edges(), 12);
  EXPECT_TRUE(mesh.is_closed());
  EXPECT_EQ(mesh.n_genus(), 0);
  EXPECT_EQ(mesh.n_euler(), 2);
  EXPECT_EQ(mesh.n_boundary_components(), 0);

  // check copy
  EXPECT_EQ(copy.n_faces(), 6);
  EXPECT_EQ(copy.n_vertices(), 8);
  EXPECT_EQ(copy.n_edges(), 12);
  EXPECT_TRUE(copy.is_closed());
  EXPECT_EQ(copy.n_genus(), 0);
  EXPECT_EQ(copy.n_euler(), 2);
  EXPECT_EQ(copy.n_boundary_components(), 0);
}

TEST_F(QuadMeshAPI, move_constructor) { 
  QuadMesh move = std::move(mesh);
  EXPECT_FALSE(move == mesh);

  // check if resources are moved
  EXPECT_EQ(mesh.n_faces(), 0);
  EXPECT_EQ(mesh.n_vertices(), 0);
  EXPECT_EQ(mesh.n_edges(), 0);

  // check moved object
  EXPECT_EQ(move.n_faces(), 6);
  EXPECT_EQ(move.n_vertices(), 8);
  EXPECT_EQ(move.n_edges(), 12);
  EXPECT_TRUE(move.is_closed());
  EXPECT_EQ(move.n_genus(), 0);
  EXPECT_EQ(move.n_euler(), 2);
  EXPECT_EQ(move.n_boundary_components(), 0);
}

TEST_F(QuadMeshAPI, copy_assignment) {
  QuadMesh copy;
  copy = mesh;
  EXPECT_FALSE(copy == mesh);

  // check original
  EXPECT_EQ(mesh.n_faces(), 6);
  EXPECT_EQ(mesh.n_vertices(), 8);
  EXPECT_EQ(mesh.n_edges(), 12);
  EXPECT_TRUE(mesh.is_closed());
  EXPECT_EQ(mesh.n_genus(), 0);
  EXPECT_EQ(mesh.n_euler(), 2);
  EXPECT_EQ(mesh.n_boundary_components(), 0);

  // check copy
  EXPECT_EQ(copy.n_faces(), 6);
  EXPECT_EQ(copy.n_vertices(), 8);
  EXPECT_EQ(copy.n_edges(), 12);
  EXPECT_TRUE(copy.is_closed());
  EXPECT_EQ(copy.n_genus(), 0);
  EXPECT_EQ(copy.n_euler(), 2);
  EXPECT_EQ(copy.n_boundary_components(), 0);
}

TEST_F(QuadMeshAPI, move_assignment) {
  QuadMesh move;
  move = std::move(mesh);
  EXPECT_FALSE(move == mesh);

  // check if resources have moved
  EXPECT_EQ(mesh.n_faces(), 0);
  EXPECT_EQ(mesh.n_vertices(), 0);
  EXPECT_EQ(mesh.n_edges(), 0);

  // check moved object
  EXPECT_EQ(move.n_faces(), 6);
  EXPECT_EQ(move.n_vertices(), 8);
  EXPECT_EQ(move.n_edges(), 12);
  EXPECT_TRUE(move.is_closed());
  EXPECT_EQ(move.n_genus(), 0);
  EXPECT_EQ(move.n_euler(), 2);
  EXPECT_EQ(move.n_boundary_components(), 0);
}

} // namespace test_dolphinmesh_api