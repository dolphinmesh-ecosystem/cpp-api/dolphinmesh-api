from conans import ConanFile, CMake

class ConanPackage(ConanFile):
    name = "dolphinmesh-api"
    version = "0.1.0"

    # Optional metadata
    license = "Proprietary"
    author = "Rene Hiemstra (rene.r.hiemstra@gmail.com)"
    url = "https://gitlab.com/dolphinmesh-ecosystem/cpp-api/dolphinmesh-api"
    description = "API front-end for DolphinMesh"
    topics = ("half-edge-mesh", "c-api", "c++11-api")

    settings = "os", "compiler", "build_type", "arch"

    # get sources from the remote
    scm = {
        "type": "git",
        "url": "git@gitlab.com:dolphinmesh-ecosystem/cpp-api/dolphinmesh-api.git",
        "revision": "master"
    }

    generators = "cmake_find_package"

    def requirements(self):
        self.requires("gtest/cci.20210126")
        self.requires("dolphinmesh/0.2.0@dolphinmesh-ecosystem+core+dolphinmesh/stable")

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()
        cmake.install()

    def package(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = self.collect_libs()