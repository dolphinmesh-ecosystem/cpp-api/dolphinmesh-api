#pragma once

#include <assert.h>
#include <cstdint>
#include <array>

template<typename T>
class ArrayView { 

  T* data;
  int len;

public:

  ArrayView() {}

  ArrayView(T *first, const int len) : data(first), len(len) {}

  T* get_data_ptr() { return data; }

  const T& operator[](const int i) const { 
	assert(-1 < i < len && "Index out of range.");
    return data[i];
  }

  T &operator[](const int i) {
    assert(-1 < i < len && "Index out of range.");
    return data[i];
  }

  friend const bool operator==(const ArrayView &a, const ArrayView &b) {
    return (a.data == b.data) && (a.len == b.len);
  }
};


class Vertex {

public:

  int32_t index;
  ArrayView<double> position;

  Vertex() = default;

  Vertex(const int32_t& i) : index(i) {}

  Vertex(const int32_t &i, const ArrayView<double> &p)
      : index{i}, position{p} {}

  friend const bool operator==(const Vertex& a, const Vertex& b) {
    return (a.position == b.position && a.index == b.index); 
  }
};


class Face {

  const int32_t nv;
  Vertex vertices[5];


public:
  int32_t index;

  Face(const int32_t &i, const Vertex &a, const Vertex &b, const Vertex &c)
      : index(i), nv(3) {
    vertices[0] = a;
    vertices[1] = b;
    vertices[2] = c;
  }

  Face(const int32_t &i, const Vertex &a, const Vertex &b, const Vertex &c,
       const Vertex &d)
      : index(i), nv(4) {
    vertices[0] = a;
    vertices[1] = b;
    vertices[2] = c;
    vertices[3] = d;
  }

  Face(const int32_t &i, const Vertex &a, const Vertex &b, const Vertex &c,
       const Vertex &d, const Vertex &e)
      : index(i), nv(5) {
    vertices[0] = a;
    vertices[1] = b;
    vertices[2] = c;
    vertices[3] = d;
    vertices[4] = e;
  }

  const int32_t n_sides() { return nv; }

  Vertex get_vertex(const int32_t& i) const {
    assert(-1 < i < nv && "Index out of range.");
    return vertices[i];
  }

};


class Edge {

  Vertex vertices[2];

public:

  const int32_t index;

  Edge(const int32_t &i, const Vertex &a, const Vertex &b)
      : index(i), vertices{a, b} {
  }

  Vertex& get_vertex(const int32_t &i) {
    assert(-1 < i < 2 && "Index out of range.");
    return vertices[i];
  }
};