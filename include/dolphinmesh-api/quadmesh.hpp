#pragma once

#include "quadmesh.h"
#include "error.hpp"

#include "dolphinmesh-api/primitives.hpp"

class QuadMesh {

private:
  quadmesh_t _opaque;
 
public:
  /// <summary>
  /// Default constructor
  /// </summary>
  QuadMesh() : _opaque(quadmesh_default_constructor(ThrowOnError{})) {}

  /// <summary>
  /// Constructor
  /// </summary>
  /// <param name="n_faces"></param>
  /// <param name="n_vertices"></param>
  /// <param name="storage_factor"></param>
  QuadMesh(const std::size_t n_faces, const std::size_t n_vertices,
              const double storage_factor)
      : _opaque(quadmesh_construct_f1(n_faces, n_vertices, storage_factor,
                                         ThrowOnError{})) {}

  // destructor
  ~QuadMesh() { quadmesh_destruct(_opaque);
  }

  /// <summary>
  /// Copy constructor. Performs a deep-copy.
  /// </summary>
  /// <param name=""></param>
  QuadMesh(const QuadMesh &other)
      : _opaque(quadmesh_copy_constructor(other._opaque, ThrowOnError{})){};

  /// <summary>
  /// Move constructor. Moves the resourses of `other` to `this` mesh.
  /// </summary>
  /// <param name="other"></param>
  QuadMesh(QuadMesh &&other)
      : _opaque(quadmesh_move_constructor(other._opaque, ThrowOnError{})){};

  /// <summary>
  /// Copy assignment operator
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  QuadMesh &operator=(const QuadMesh &other) {
    quadmesh_copy_assignment(this->_opaque, other._opaque, ThrowOnError{});
    return *this;
  }

  /// <summary>
  /// Move assignment operator
  /// </summary>
  /// <param name=""></param>
  /// <returns></returns>
  QuadMesh &operator=(QuadMesh &&other) {
    quadmesh_move_assignment(this->_opaque, other._opaque, ThrowOnError{});
    return *this;
  }

  /// <summary>
  /// Check if two meshes are strongly equal, meaning they share exactly
  /// the same data.
  /// </summary>
  /// <param name="mesh"></param>
  /// <param name="other"></param>
  /// <returns></returns>
  friend bool operator==(const QuadMesh &mesh, const QuadMesh &other) {
    return quadmesh_same_objects(mesh._opaque, other._opaque,
                                    ThrowOnError{});
  }

  /// <summary>
  /// Return number of mesh vertices
  /// </summary>
  /// <returns></returns>
  const uint32_t n_vertices() const { return quadmesh_n_vertices(_opaque);
  }

  /// <summary>
  /// Return number of mesh faces
  /// </summary>
  /// <returns></returns>
  const uint32_t n_faces() const { return quadmesh_n_faces(_opaque);
  }

  /// <summary>
  /// Return number of mesh edges
  /// </summary>
  /// <returns></returns>
  const uint32_t n_edges() const { return quadmesh_n_edges(_opaque);
  }

  /// <summary>
  /// Return number of boundary components
  /// </summary>
  /// <returns></returns>
  const uint32_t n_boundary_components() const {
    return quadmesh_n_boundary_components(_opaque);
  }

  /// <summary>
  /// Compute the surface genus
  /// </summary>
  /// <returns></returns>
  const uint32_t n_genus() const { return quadmesh_n_genus(_opaque);
  }

  /// <summary>
  /// Compute the mesh Euler number
  /// </summary>
  /// <returns></returns>
  const uint32_t n_euler() const { return quadmesh_n_euler(_opaque); }

  /// <summary>
  /// Returns true if the mesh is a closed surface
  /// </summary>
  /// <returns></returns>
  const bool is_closed() const { return quadmesh_is_closed(_opaque); }

  /// <summary>
  /// Add mesh vertex
  /// </summary>
  /// <param name="x"></param>
  /// <param name="y"></param>
  /// <param name="z"></param>
  void add_vertex(double x, double y, double z) {
    return quadmesh_add_vertex(_opaque, x, y, z, ThrowOnError{});
  }

  /// <summary>
  /// Add mesh face
  /// </summary>
  /// <param name="a"></param>
  /// <param name="b"></param>
  /// <param name="c"></param>
  /// <param name="d"></param>
  void add_face(int64_t a, int64_t b, int64_t c, int64_t d) {
    return quadmesh_add_face_4(this->_opaque, a, b, c, d, ThrowOnError{});
  }

  /// <summary>
  /// Finalize initialization of mesh object. This function needs to be called
  /// after all vertices and faces have been added and before the mesh is used
  /// in further operations.
  /// </summary>
  void finalize() { return quadmesh_finalize(_opaque, ThrowOnError{});
  }

    /// <summary>
  /// Get vertex
  /// </summary>
  /// <param name="index"></param>
  /// <returns></returns>
  Vertex get_vertex(const int32_t index) const {
    point3d pt;
    quadmesh_get_vertex_position(_opaque, index, &pt, ThrowOnError{});
    Point3d point;
    point.set_data_ptr(pt.data);
    return Vertex(index, point);
  }

  /// <summary>
  /// Get Face
  /// </summary>
  /// <param name="index"></param>
  /// <returns></returns>
  Face get_face(const int32_t index) const {
    int32_t face_vertices[4];
    quadmesh_get_face_vertices_4(_opaque, index, face_vertices, ThrowOnError{});
    Vertex a = get_vertex(face_vertices[0]);
    Vertex b = get_vertex(face_vertices[1]);
    Vertex c = get_vertex(face_vertices[2]);
    Vertex d = get_vertex(face_vertices[3]);
    return Face(index, a, b, c, d);
  }

  /// <summary>
  /// Get Edge
  /// </summary>
  /// <param name="index"></param>
  /// <returns></returns>
  Edge get_edge(const int32_t index) const {
    int32_t edge_vertices[2];
    quadmesh_get_edge_vertices(_opaque, index, edge_vertices, ThrowOnError{});
    Vertex a = get_vertex(edge_vertices[0]);
    Vertex b = get_vertex(edge_vertices[1]);
    return Edge(index, a, b);
  }


};