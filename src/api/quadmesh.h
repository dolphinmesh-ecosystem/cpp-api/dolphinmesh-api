#pragma once

#include <stdint.h>
#include "api_export.h"

#include "error.h"
#include "primitives.h"

#ifdef __cplusplus
extern "C" {
#endif

///
/// Alias to polgonmesh* 
///
typedef struct quadmesh *quadmesh_t;

///
/// Constructors 
///
API_EXPORT
quadmesh_t quadmesh_default_constructor(api_error_t *);

API_EXPORT
quadmesh_t quadmesh_construct_f1(int32_t, int32_t, double, api_error_t *);

API_EXPORT
quadmesh_t quadmesh_copy_constructor(quadmesh_t, api_error_t *);

API_EXPORT
quadmesh_t quadmesh_move_constructor(quadmesh_t, api_error_t *);

API_EXPORT
void quadmesh_copy_assignment(quadmesh_t, quadmesh_t, api_error_t *);

API_EXPORT
void quadmesh_move_assignment(quadmesh_t, quadmesh_t, api_error_t *);

///
/// Destructo object
///
API_EXPORT
void quadmesh_destruct(quadmesh_t);

///
/// Mesh properties
///
API_EXPORT
bool quadmesh_same_objects(quadmesh_t, quadmesh_t, api_error_t *);

API_EXPORT
int32_t quadmesh_n_vertices(quadmesh_t);

API_EXPORT
int32_t quadmesh_n_faces(quadmesh_t);

API_EXPORT
int32_t quadmesh_n_edges(quadmesh_t);

API_EXPORT
int32_t quadmesh_n_boundary_components(quadmesh_t);

API_EXPORT
int32_t quadmesh_n_genus(quadmesh_t);

API_EXPORT
int32_t quadmesh_n_euler(quadmesh_t);

API_EXPORT
bool quadmesh_is_closed(quadmesh_t);

///
/// Add mesh vertex
///
API_EXPORT
void quadmesh_add_vertex(quadmesh_t, double, double, double, api_error_t *);

///
/// Add mesh face
///
API_EXPORT
void quadmesh_add_face_4(quadmesh_t, int32_t, int32_t,
                            int32_t, int32_t, api_error_t *);

///
/// Finalize mesh object
///
API_EXPORT
void quadmesh_finalize(quadmesh_t, api_error_t *);

///
/// Get the position vector corresponding to given vertex
///
API_EXPORT
void quadmesh_get_vertex_position(quadmesh_t, int32_t, point3d_t, api_error_t *);

///
/// Get the vertex indices of a face
///
API_EXPORT
void quadmesh_get_face_vertices_4(quadmesh_t, int32_t, int32_t *, api_error_t *);

///
/// Get the vertex indices of an edge
///
API_EXPORT
void quadmesh_get_edge_vertices(quadmesh_t, int32_t, int32_t *, api_error_t *);


#ifdef __cplusplus
} // extern "C"
#endif
