#include "dolphinmesh/halfedgemesh.hpp"

#include "api/error.h"
#include <stdexcept>


extern "C" {

struct api_error {
  std::string message;
};

const char *error_message(api_error_t error) { return error->message.c_str(); }

void error_destruct(api_error_t error) { delete error; }


} // extern "C"


/// Returns true if fn executed without throwing an error, false otherwise.
/// If calling fn threw an error, capture it in *out_error.
template <typename Fn> bool translateExceptions(api_error_t *out_error, Fn &&fn) {
  try {
    fn();
  } catch (const std::exception &e) {
    *out_error = new api_error{e.what()};
    return false;
  } catch (...) {
    *out_error = new api_error{"Unknown internal error"};
    return false;
  }
  return true;
}


#include "imp/chain_imp.hpp"

#define TRI_MESH
	#include "imp/halfedgemesh_imp.hpp"
#undef TRI_MESH

//#define QUAD_MESH
//	#include "implementation.hpp"
//#undef QUAD_MESH
//
//#define POLYGON_MESH
//	#include "implementation.hpp"
//#undef POLYGON_MESH