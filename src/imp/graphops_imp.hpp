#include "dolphinmesh-api/chain.h"

#include "dolphinmesh/chain.hpp"

///
/// Here comes the bottom cpp interface into the library
///
extern "C" {

struct chain {

  dolphinmesh::Chain<double> actual;

  chain(const dolphinmesh::Chain<double> &c) : actual{c} {};
};

void chain_destruct(chain_t c) { delete c; }

bool chain_is_closed(chain_t c) { return c->actual.is_closed(); }

int32_t chain_n_vertices(chain_t c) { return c->actual.n_vertices(); }

int32_t chain_n_edges(chain_t c) { return c->actual.n_halfedges(); }

double *chain_get_vertex_position_ptr(chain_t c, int32_t index) {
  auto* vertex = c->actual.get_vertex(index);
  return vertex->position.data();
}

} // extern "C"