#if defined POLYGON_MESH
    #include "dolphinmesh-api/polygonmesh.h"
#elif defined TRI_MESH
    #include "trimesh.h"
#elif defined QUAD_MESH
    #include "dolphinmesh-api/quadmesh.h"
#endif

#include <iostream>

///
/// Here comes the bottom cpp interface into the library
///
extern "C" {

#if defined POLYGON_MESH
    #define FUNCTION_NAME(x) polygonmesh##_##x
    #define HALFEDGEMESH polygonmesh
    #define HALFEDGEMESH_T polygonmesh_t
    #define DOLPHIN_MESH_TYPE dolphinmesh::PolygonMesh<double>

#elif defined TRI_MESH
    #define FUNCTION_NAME(x) trimesh##_##x
    #define HALFEDGEMESH trimesh
    #define HALFEDGEMESH_T trimesh_t
    #define DOLPHIN_MESH_TYPE dolphinmesh::TriMesh<double>
    
#elif defined QUAD_MESH
    #define FUNCTION_NAME(x) quadmesh##_##x
    #define HALFEDGEMESH quadmesh
    #define HALFEDGEMESH_T quadmesh_t
    #define DOLPHIN_MESH_TYPE dolphinmesh::QuadMesh<double>

#endif


struct HALFEDGEMESH {
  DOLPHIN_MESH_TYPE actual;

  HALFEDGEMESH() : actual(){};
  HALFEDGEMESH(int32_t n_faces, int32_t n_vertices, double storage_factor = 1.1)
      : actual(n_faces, n_vertices, storage_factor) {}
  HALFEDGEMESH(const HALFEDGEMESH &other) : actual(other.actual) {}
  HALFEDGEMESH(HALFEDGEMESH &&other) : actual(std::move(other.actual)) {}
};

///
/// Constructors 
///
HALFEDGEMESH_T FUNCTION_NAME(default_constructor)(api_error_t *out_error) {
  HALFEDGEMESH_T result = nullptr;
  translateExceptions(out_error, [&] {
    result = std::make_unique<HALFEDGEMESH>().release();
  });
  return result;
}

HALFEDGEMESH_T FUNCTION_NAME(construct_f1)(int32_t n_faces, int32_t n_vertices,
                                        double storage_factor,
                                        api_error_t *out_error) {
  HALFEDGEMESH_T result = nullptr;
  translateExceptions(out_error, [&] {
    result = std::make_unique<HALFEDGEMESH>(n_faces, n_vertices, storage_factor).release();
  });
  return result;
}

HALFEDGEMESH_T FUNCTION_NAME(copy_constructor)(HALFEDGEMESH_T other,
                                            api_error_t *out_error) {
  HALFEDGEMESH_T result = nullptr;
  translateExceptions(out_error, [&] {
    result = std::make_unique<HALFEDGEMESH>(*other).release(); // copy constructor
  });
  return result;
}

HALFEDGEMESH_T FUNCTION_NAME(move_constructor)(HALFEDGEMESH_T other, api_error_t *out_error) {
  HALFEDGEMESH_T result = nullptr;
  translateExceptions(out_error, [&] { 
    result = std::make_unique<HALFEDGEMESH>(std::move(*other)).release(); // move constructor
    other = nullptr;
  });
  return result;
}

void FUNCTION_NAME(copy_assignment)(HALFEDGEMESH_T result, HALFEDGEMESH_T other,
                                  api_error_t *out_error) {
  translateExceptions(out_error, [&] { 
    result->actual = other->actual; // copy assignment 
  });
}

void FUNCTION_NAME(move_assignment)(HALFEDGEMESH_T result, HALFEDGEMESH_T other, api_error_t *out_error) {
  translateExceptions(out_error, [&] {
    result->actual = std::move(other->actual); // move assignment
    other = nullptr;
  });
}

///
/// Destruct mesh object 
///
void FUNCTION_NAME(destruct)(HALFEDGEMESH_T mesh) { delete mesh; }

///
/// Mesh properties
///
bool FUNCTION_NAME(same_objects)(HALFEDGEMESH_T mesh, HALFEDGEMESH_T other,
                               api_error_t *out_error) {
  bool result = false;
  translateExceptions(out_error, [&] {
    bool a = &mesh->actual.position == &other->actual.position;
    bool b = &mesh->actual.vertices == &other->actual.vertices;
    bool c = &mesh->actual.faces == &other->actual.faces;
    bool d = &mesh->actual.edges == &other->actual.edges;
    bool e = &mesh->actual.halfedges == &other->actual.halfedges;
    bool f = &mesh->actual.boundaries == &other->actual.boundaries;
    bool g = mesh->actual.is_initialized() == other->actual.is_initialized();
    result = (a && b && c && d && e && f && g);
  });
  return result;
}

int32_t FUNCTION_NAME(n_vertices)(HALFEDGEMESH_T mesh) {
  return mesh->actual.n_vertices();
}

int32_t FUNCTION_NAME(n_faces)(HALFEDGEMESH_T mesh) {
  return mesh->actual.n_faces();
}

int32_t FUNCTION_NAME(n_edges)(HALFEDGEMESH_T mesh) {
  return mesh->actual.n_edges();
}

API_EXPORT
int32_t FUNCTION_NAME(n_boundary_components)(HALFEDGEMESH_T mesh) {
  return mesh->actual.n_boundary_components();
}

API_EXPORT 
int32_t FUNCTION_NAME(n_genus)(HALFEDGEMESH_T mesh) {
  return mesh->actual.n_genus();
}

API_EXPORT
int32_t FUNCTION_NAME(n_euler)(HALFEDGEMESH_T mesh) {
  return mesh->actual.n_euler();
}

API_EXPORT
bool FUNCTION_NAME(is_closed)(HALFEDGEMESH_T mesh) {
  return mesh->actual.is_closed();
}


///
/// Add mesh vertex
///
void FUNCTION_NAME(add_vertex)(HALFEDGEMESH_T mesh, double x, double y, double z,
                            api_error_t *out_error) {
  translateExceptions(out_error, [&] { 
    mesh->actual.add_vertex(x, y, z); 
  });
}

///
/// Add mesh face
///
#if defined POLYGON_MESH || defined TRI_MESH
void FUNCTION_NAME(add_face_3)(HALFEDGEMESH_T mesh, int32_t a, 
                            int32_t b, int32_t c, 
                            api_error_t *out_error) {
  translateExceptions(out_error, [&] { 
    mesh->actual.add_face(a, b, c);
  });
}
#endif

#if defined POLYGON_MESH || defined QUAD_MESH
void FUNCTION_NAME(add_face_4)(HALFEDGEMESH_T mesh, int32_t a, 
                            int32_t b, int32_t c, 
                            int32_t d, error_t *out_error) {
  translateExceptions(out_error, [&] { 
      mesh->actual.add_face(a, b, c, (std::size_t)d); 
  });
}
#endif

#if defined POLYGON_MESH
void FUNCTION_NAME(add_face_5)(HALFEDGEMESH_T mesh, int32_t a,
                            int32_t b, int32_t c,
                            int32_t d, int32_t e,
                            error_t *out_error) {
  translateExceptions(out_error, [&] {
    mesh->actual.add_face(a, b, c, (std::size_t)d, (std::size_t)e);
  });
}
#endif

///
/// finalize object
/// 
void FUNCTION_NAME(finalize)(HALFEDGEMESH_T mesh, api_error_t *out_error) {
  
  translateExceptions(out_error, [&] {
    try {
      mesh->actual.finalize();
    } catch (const std::exception &e) {
      std::cerr << "Error during finalization of mesh: " << e.what() << std::endl;
    }
  });
}

///
/// Get a pointer to the position of given vertex
///
double* FUNCTION_NAME(get_vertex_position_ptr)(HALFEDGEMESH_T mesh, int32_t index) {
  return mesh->actual.position[index].data();
}

void FUNCTION_NAME(get_vertex_position)(HALFEDGEMESH_T mesh,
                                               int32_t index,
                                               double* position,
                                               api_error_t *out_error) {
  translateExceptions(out_error, [&] { 
    double* p = mesh->actual.position[index].data();
    position[0] = *p;
    position[1] = *(p+1);
    position[2] = *(p+2);
  });
}

void FUNCTION_NAME(get_edge_vertices)(HALFEDGEMESH_T mesh, int32_t index,
                                      int32_t *edge_vertices,
                                      api_error_t *out_error) {
  translateExceptions(out_error, [&] {
    auto* e = &mesh->actual.edges[index];
    edge_vertices[0] = mesh->actual.index(*e->from_vertex());
    edge_vertices[1] = mesh->actual.index(*e->to_vertex());
  });
}

void FUNCTION_NAME(get_face_vertices_imp)(HALFEDGEMESH_T mesh, int32_t index,
                                          int32_t *face_vertices) {
  auto *first = mesh->actual.faces[index].halfedge;
  auto *he = first;
  int count = 0;
  do {
    face_vertices[count] = mesh->actual.index(*he->vertex);
    ++count;
    he = he->prev;
  } while (he != first);
}

chain_t FUNCTION_NAME(get_boundary_component)(HALFEDGEMESH_T mesh, int32_t index, api_error_t *out_error) {

  chain_t result = nullptr;
  translateExceptions(out_error, [&] {
    // get boundary component
    auto boundary = mesh->actual.boundaries[index];
    
    // initialize chain
    dolphinmesh::Chain<double> c;
    c.set_backward_direction();

    // fill Chain
    for (dolphinmesh::Halfedge<double> *he : boundary.halfedges()) {
      c.push_back(he);
    }

    // create chain
    result = std::make_unique<chain>(c).release();
  });
  return result;
}


///
/// Get a pointer to the position of given vertex
///
#if defined POLYGON_MESH || defined TRI_MESH
void FUNCTION_NAME(get_face_vertices_3)(HALFEDGEMESH_T mesh, int32_t index,
                                        int32_t *face_vertices,
                                        api_error_t *out_error) {
  translateExceptions(out_error, [&] { 
    FUNCTION_NAME(get_face_vertices_imp)(mesh, index, face_vertices);
  });
}
#endif

#if defined POLYGON_MESH || defined QUAD_MESH
void FUNCTION_NAME(get_face_vertices_4)(HALFEDGEMESH_T mesh, int32_t index,
                                        int32_t *face_vertices,
                                        error_t *out_error) {
  translateExceptions(out_error, [&] {
    FUNCTION_NAME(get_face_vertices_imp)(mesh, index, face_vertices);
  });
}
#endif

#if defined POLYGON_MESH

int32_t FUNCTION_NAME(n_face_vertices)(HALFEDGEMESH_T mesh, int32_t index, error_t *out_error) {
  const auto& face = mesh->actual.faces[index];
  return face.n_sides();
}

void FUNCTION_NAME(get_face_vertices_5)(HALFEDGEMESH_T mesh, int32_t index,
                                        int32_t *face_vertices,
                                        error_t *out_error) {
  translateExceptions(out_error, [&] {
    FUNCTION_NAME(get_face_vertices_imp)(mesh, index, face_vertices);
  });
}
#endif







// undefine the macros defined at the top
#undef FUNCTION_NAME
#undef HALFEDGEMESH
#undef HALFEDGEMESH_T
#undef DOLPHIN_MESH_TYPE

}